package com.week1tasks;

public class vectorFuncs {
	public long scalarSum(int[] a, int[] b) {
		long scalarSum = 0;
		int length = Math.max(a.length,b.length);
		for(int i=0;i<length;i++) {
			scalarSum += a[i] * b[i];
		}
		return scalarSum;
	}

	private void sortArr(int[] array) {
		for(int i=0;i<array.length-1;i++) {
			int min = array[i], position = i;
			
			for(int j = i + 1; j<array.length;j++) {
				if(min > array[j]) {
					min = array[j];
					position = j;
				}
			}
			int temp = array[i];
			array[i] = array[position];
			array[position] = temp;
		}
	}
	public long maxScalarSum(int[] a, int[] b) {
		sortArr(a);
		System.out.println(a);
		sortArr(b);
		System.out.println(b);
		return scalarSum(a,b);
	}


}

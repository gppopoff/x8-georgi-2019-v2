package com.week1tasks;
import java.util.List; // import just the List interface
import java.util.ArrayList; // import just the ArrayList class
import java.lang.Math;

public class minimalMultiple {
	public long minMulti(int n) {
		long result = 1;
		
		List<Integer> ints = new ArrayList<Integer>();
		ints.add(2);
		numberChecks testPrime = new numberChecks();
		for(int i=3;i<=n;i+=2) {
			if(testPrime.isPrime(i)) {
				ints.add(i);
			}
		}
		for(int i=0;i<ints.size();i++) {
			for(int j = 1; ;j++) {
				if(n < Math.pow(ints.get(i),j)) {
					result *= Math.pow(ints.get(i),j-1);
					break;
				}
			}
		}
		return result;
	}
}

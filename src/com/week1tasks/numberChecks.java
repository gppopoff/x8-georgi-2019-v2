package com.week1tasks;

public class numberChecks {
	public boolean isPrime(int n) {
		if(n%2 == 0 && n!=2 || n == 1) {
			return false;
		}
		for(int i=3;i<n/2;i+=2) {
			if(n%i == 0) {
				return false;
			}
		}
		return true;
	}
	public boolean isOdd(int n) {
		if(n%2 == 0) {
			return false;
		}
		return true;
	}
	public long recFac(long n) {
		if(n == 1) {
			return 1;
		}else {
			return n*recFac(n-1);
		}
	}
	
	public long Fac(long n) {
		long result = 1;
		for(int i=2;i<=n;i++) {
			result*=i;
		}
		return result;
	}
	public long kFac(int n,int k) {
		long result = n;
		for(int i=0;i<k;i++) {
			result = recFac(result);
		}
		return result;
	}
}

package com.week1tasks;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class imagekr {
	public void convertToGreyscale(String imgPath) {
		try {
			BufferedImage img = null;

			try {
			    img = ImageIO.read(new File(imgPath));
			} 
			catch (IOException e) {
			    //e.printStackTrace();
			}
			
			for(int i = 0 ; i < 500 ; i++) {
				for(int j = 0 ; j < 333 ; j++) {
					Color orig = new Color(img.getRGB(i, j));
					int col = (orig.getRed() + orig.getBlue() + orig.getGreen() )/3;
					Color newOne = new Color(col,col,col);
					img.setRGB(i,j,newOne.getRGB());
				}
			}
			
			File outputfile = new File("saved.jpeg");
		    ImageIO.write(img, "jpeg", outputfile);

		}
		catch(IOException e) {
		}
	}
	
}

package com.week1tasks.exception;

public class EmptyArrayExeption extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EmptyArrayExeption() {
		super();
		// TODO Auto-generated constructor stub
	}

	public EmptyArrayExeption(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public EmptyArrayExeption(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public EmptyArrayExeption(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public EmptyArrayExeption(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}

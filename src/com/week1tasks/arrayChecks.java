package com.week1tasks;

import java.util.HashMap;

import com.week1tasks.exception.*;

public class arrayChecks {
	public int minElement(int... array) {
		if(array.length == 0) {
			System.out.println("No elements in array");
			return 0;
		}
		int min = array[0];
		for(int i=1;i<array.length;i++) {
			if(min > array[i]) {
				min = array[i];
			}
		}
		
		return min;
	}
	public int kthMin(int k, int[] array) {
		for(int i=0;i<array.length-1;i++) {
			int min = array[i], position = i;
			
			for(int j = i + 1; j<array.length;j++) {
				if(min > array[j]) {
					min = array[j];
					position = j;
				}
			}
			int temp = array[i];
			array[i] = array[position];
			array[position] = temp;
		}
		
		return array[k-1];
	}
	public int getOddOccurrence(int... array) throws EmptyArrayExeption {
		if(array.length == 0) {
			System.out.println("No elements in array");
			throw new EmptyArrayExeption();
		}
		for(int i=0;i<array.length-1;i++) {
			int min = array[i], position = i;
			
			for(int j = i + 1; j<array.length;j++) {
				if(min > array[j]) {
					min = array[j];
					position = j;
				}
			}
			int temp = array[i];
			array[i] = array[position];
			array[position] = temp;
		}
		int count = 1;
		for(int i=0; i<array.length - 1;i++) {
			if(array[i+1] == array[i]) {
				count++;
			}else {
				numberChecks check = new numberChecks();
				if(check.isOdd(count)) {
					return array[i];
				}else{
					count = 1;
				}
			}
		}
		return 0;
	}
	public int getAverage(int[] array) {
		int sum = 0;
		for(int i=0; i<array.length;i++) {
			sum += array[i];
		}
		return sum/array.length;
	}
	public int maxSpan(int[] numbers) {
		//  working !!! 
		HashMap<Integer,Integer> indexes = new HashMap<Integer,Integer>();
		int max = 0;
		for(int i=0;i<numbers.length;i++) {
			if(!indexes.containsKey(numbers[i])) {
				indexes.put(numbers[i], i);
			}else {
				if(max < i - indexes.get(numbers[i])) {
					max = i - indexes.get(numbers[i]);
				}
			}
		}
		return max + 1;   
	}
	public boolean equalSides(int[] numbers) {
		for(int i = 1 ; i < numbers.length - 1; i++) {
			int leftSum = 0;
			int rightSum = 0;
			for(int j = i-1 ; j >= 0; j--) {
				leftSum += numbers[j];
			}
			for(int j = i+1 ; j < numbers.length; j++) {
				rightSum += numbers[j];
			}
			if(leftSum == rightSum) {
				return true;
			}
		}
		return false;
	}

}

package com.week1tasks;

public class stringFuncs {
	public String reverse(String argument) {
		//StringBuilder other = new StringBuilder();
		char[] arr = argument.toCharArray();
		for(int i=0 ; i < argument.length()/2;i++) {
			char temp = arr[argument.length() - i - 1];
			arr[argument.length() - i - 1] = arr[i];
			arr[i] = temp;
		}
		String newOne = new String(arr);
		return newOne;
	}
	public String reversWords(String arg) {
		String[] arr = arg.split(" ");
		StringBuilder str = new StringBuilder();
//		for (String temp: arr){
//		      System.out.println(temp);
//		      str += reverse(temp) + " ";
//		}
		for(int i=0;i<arr.length;i++) {
			if(i!= arr.length-1) {
				str.append(reverse(arr[i]) + " ");
				//str+= reverse(arr[i]) + " ";
			}else {
				//str+= reverse(arr[i]);
				str.append(reverse(arr[i]));
			}
		}
		return str.toString();
	}
	public boolean isPalindrome(String arg) {
		char[] arr = arg.toCharArray();
		for(int i=0;i<arg.length()/2;i++) {
			if(arr[i] != arr[arg.length() - i -1]) {
				return false;
			}
		}
		return true;
	}
	public boolean isPalindrome(int number) {
		int copy = number, size = 0;
		while(copy>0) {
			copy/=10;
			size++;
		}
		int[] arr = new int[size];
		copy = number;
		for(int i=0;i<size;i++) {
			arr[i] = copy%10;
			copy/=10;
		}
		for(int i=0;i<size/2;i++) {
			if(arr[i]!= arr[arr.length -i -1]) {
				return false;
			}
		}
		return true;
	}
	public int getLargestPalindrome(int N) {
		for(int i = N; i > 0; i --) {
			if(isPalindrome(i)) {
				return i;
			}
		}
		return 0;
		
		
//		long copy = N;
//		int size = 0;
//		while(copy>0) {
//			copy/=10;
//			size++;
//		}
//		long[] arr = new long[size];
//		copy = N;
//		for(int i=0;i<size;i++) {
//			arr[i] = copy%10;
//			copy/=10;
//		} 
//		for(int i=0;i<size/2;i++) {
//			if(arr[i]>arr[arr.length -i -1]) {
//				arr[i] = arr[i] - 1;
//				arr[arr.length -i -1] = arr[i];
//			}else {
//				arr[arr.length -i -1] = arr[i];
//			}
//		}
//		//for(int i=0;i<size;i++) { System.out.println(arr[i]); }
//		long result = 0;
//		for(int i=0;i<size;i++) {
//			result += arr[i]*Math.pow(10, i);
//		}
//		return result;
	}
	public String copyChars(String input, int k) {
		char[] arr = new char[input.length()*k];
		for(int i=0;i<input.length()*k;i++) {
			int pointer = i%input.length();
			arr[i] = input.toCharArray()[pointer];
		}
		return new String(arr);
	}
	public int mentions(String word, String text) {
		char[] wordC = word.toCharArray();
		char[] textC = text.toCharArray();
		int count=0;
		if(word.length() == 0) {
			return 0;
		}
		for(int i=0; i<text.length();i++) {
			if(textC[i] == wordC[0]) {
				boolean check = true;
				for(int j=1;j<word.length();j++) {
					if(wordC[j] != textC[i+j]) {
						check = false;
						break;
					}
				}
				if(check) {
					count++;
				}
			}
		}
		return count;
	}
	public String decodeUrl(String input) {
		String newOne = input.replaceAll("%3D", "?");
		newOne = newOne.replace("%20", " ");
		newOne = newOne.replace("%2F", "/");
		newOne = newOne.replace("%3A", ":");
		
		/*String[] arr = input.split("%20");
		String result = new String();
		for(int i=0;i<arr.length;i++) {
			result += arr[i];
			if(i != arr.length -1) {
				result += " ";
			}
		}
		String[] arr1 = result.split("%3A");
		String result1 = new String();
		for(int i=0;i<arr1.length;i++) {
			result1 += arr1[i];
			if(i != arr1.length -1) {
				result1 += ":";
			}
		}
		String[] arr2 = result1.split("%3D");
		String result2 = new String();
		for(int i=0;i<arr2.length;i++) {
			result2 += arr2[i]; 
			if(i != arr2.length -1) {
				result2 += "?";
			}
		}
		String[] arr3 = result2.split("%2F");
		String result3 = new String();
		for(int i=0;i<arr3.length;i++) {
			result3 += arr3[i]; 
			if(i != arr3.length -1) {
				result3 += "/";
			}
		}
		return result3;
		*/
		return newOne;
	}
	public int sumOfNumbers(String input) {
		Integer sum = 0;
		if(input.length() == 0) {
			return 0;
		}
		String[] arr = input.split("[^0-9]+");
		for(String temp : arr) {
			if(!(temp.length() == 0)) {
				sum += Integer.parseInt(temp);
			}
			
			//System.out.println(sum + "<-");
		}
		return sum;
	}
	public boolean isAnagram(String A,String B) {
		int[] a = new int[52];
		int[] b = new int[52];
		char[] AA = A.toCharArray();
		char[] BB = B.toCharArray();
		for(int i=0;i<AA.length;i++) {
			int symb = AA[i];
			if(symb >= 97 && symb <= 122) {
				symb = symb - 97 + 26;
				a[symb] ++;
			}else if(symb >= 65 && symb <= 90) {
				symb = symb - 65;
				a[symb] ++;
			}
		}
		for(int i=0;i<BB.length;i++) {
			int symb = BB[i];
			if(symb >= 97 && symb <= 122) {
				symb = symb - 97 + 26;
				b[symb] ++;
			}else if(symb >= 65 && symb <= 90) {
				symb = symb - 65;
				b[symb] ++;
			}
		}
		for(int i=0;i<52;i++) {
			if(a[i]!=b[i]) {
				return false;
			}
		}
		return true;
	}
	public boolean hasAnagramOf(String A,String B) {
//		for(int i=0;i<A.length()-1;i++) {
//			for(int j=i+1;j<A.length();j++) {
//				if(isAnagram(A.substring(i, j+1),B)) {
//					return true;
//				}
//			}
//		}
//		return false;
		A = A.replaceAll("[^a-zA-Z]+", "");
		B = B.replaceAll("[^a-zA-Z]+", "");
		System.out.println(A);
		for(int i=0;i<A.length()-B.length()+1;i++) {
			if(isAnagram(A.substring(i, i+B.length()),B)) {
				return true;
			}
		}
		return false;
	}
	public int[] histogram(short[][] image) {
		int[] counter = new int[256];
		for(int i=0;i<image.length;i++) {
			for(int j=0;j<image[i].length;j++) {
				counter[image[i][j]]++;
			}
		}
		return counter;
	}
	public int[][] rescale(int[][] original, int newWidth, int newHeight){
		float propH = ((float)original.length) / newHeight;
		float propW = ((float)original[0].length) / newWidth;
		int[][] newOne = new int[newHeight][newWidth];
		System.out.println(propH);
		float sol = (float)0.1;
		for(int i=0 ; i<newHeight ; i++) {
			for(int j=0 ; j<newWidth ; j++) {
				int newH ;
//				if(Math.round(propH * i) >= original.length) {
//					newH = Math.round(propH * i);
//					newH--;
//				}else {
//					newH = Math.round(propH * i);
//				}
				int newW ;
//				if( Math.round(propW * j - 0.1) >= original[0].length) {
//					newW = Math.round(propW * j);
//					newW--;
//				}else {
//					newW = Math.round(propW * j);
//				}
				if((int)Math.floor((propH+sol) * i) >= original.length) {
					newH = (int)Math.floor((propH) * i);
				}else {
					newH = (int)Math.floor((propH+sol) * i);
				}
				if((int)Math.floor((propW+sol) * j) >= original[0].length) {
					newW = (int)Math.floor((propW) * j);
				}else {
					newW = (int)Math.floor((propW+sol) * j);
				}
				
				//System.out.println(newH + " " + newW);
				newOne[i][j] = original[newH][newW];
			}
		}
		for(int i=0 ; i<newHeight ; i++) {
			for(int j=0 ; j<newWidth; j++) {
				System.out.print(newOne[i][j]+" ");
			}
			System.out.println();
		}
		int[][] extraOne = new int[newOne.length+2][newOne[0].length+2];
		for(int i=1 ; i<=newHeight ; i++) {
			for(int j=1 ; j<=newWidth ; j++) {
				extraOne[i][j] = newOne[i-1][j-1];
			}
		}
		for(int i=0 ; i<=newHeight +1 ; i++) {
			for(int j=0 ; j<=newWidth +1; j++) {
				System.out.print(extraOne[i][j]+" ");
			}
			System.out.println();
		}
		for(int i=1 ; i<=newHeight ; i++) {
			for(int j=1 ; j<=newWidth ; j++) {
				int sum = extraOne[i-1][j-1] + extraOne[i-1][j] + extraOne[i-1][j+1]
						+ extraOne[i][j-1] + extraOne[i][j] + extraOne[i][j+1]
						+ extraOne[i+1][j-1] + extraOne[i+1][j]+ extraOne[i+1][j+1];
				if( (i == 1 && j == 1) || (i == newHeight && j == newWidth) 
						|| (i == newHeight && j == 1) || (i == 1 && j == newWidth) ) {
					sum = sum / 4;
				}else if(i == 1 || j == 1 || i == newHeight || j == newWidth) {
					sum = sum / 6;
				}else {
					sum = sum / 9;
				}
				newOne[i-1][j-1] = sum;
			}
		}
		
		
		return newOne;
	}
	
}



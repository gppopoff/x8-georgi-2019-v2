package com.queues;

import java.util.EmptyStackException;

public class arrayQueue {
	  // *** fields ***
    private static final int INITSIZE = 4;  // initial array size
    private Object[] items; // the items in the queue
    private int numItems;   // the number of items in the queue
    private int rearIndex;
    private int frontIndex;

  //*** methods ***

  // constructor
    public arrayQueue() {
    	items = new Object[INITSIZE];
    	numItems = 0;
    	frontIndex = 0;
    	rearIndex = 0;
    }      

  // add items
    public void enqueue(Object ob) {
    	// check for full array and expand if necessary
        if (items.length == numItems) {
        	Object[] temp = new Object[items.length*2];
        	for(int i=frontIndex; i<items.length; i++) {
        		temp[i - frontIndex] = items[i];
        	}
        	for(int i=0;i<frontIndex;i++) {
        		temp[items.length - frontIndex + i] = items[i];
        	}
        	frontIndex = 0;
        	rearIndex = items.length;
        	items = temp;
        }
        if(rearIndex == items.length) {
        	rearIndex = 0;
        }
        items[rearIndex] = ob;
        rearIndex++;
        numItems++;
        
	}

  // remove items
    public Object dequeue() throws EmptyStackException {
    	if(empty()) {
    		throw new EmptyStackException();
    	}else {
    		numItems--;
    		if(frontIndex == items.length) {
    			frontIndex = 0;
    		}
    		frontIndex++;
    		return items[frontIndex - 1];
    	}
    }
    

  // other methods
    public int size() {
    	return numItems;
    }      
    public boolean empty() {
    	return numItems == 0;
    } 
}

package com.lists;

public class Listnode {
	//*** fields ***
    private Object data;
    private Listnode next;

  //*** methods ***
    // 2 constructors
    public Listnode(Object d) {
	this(d, null);
    }

    public Listnode(Object d, Listnode n) {
	data = d;
	next = n;
    }
    
    // access to fields
    public Object getData() {
        return data;
    }

    public Listnode getNext() {
        return next;
    }

    // modify fields
    public void setData(Object ob) {
        data = ob;
    }

    public void setNext(Listnode n) {
        next = n;
    }
}

package com.week3.arrayOperations;

import java.util.HashMap;

public class NotSortedArray {
	
	int findMinMissing(int[] array, int n) {
		if(array.length >= n) {			
			boolean[] exist = new boolean[n];
			
			for(int i = 0 ; i < array.length ; i++) {
				exist[array[i] - 1] = true;
			}
			int i = 0;
			while(i < exist.length && exist[i++] == true) {}
			
			if(i == exist.length ) {
				i++;
			}
			
			
			return i;
		} else {
			boolean[] exist = new boolean[array.length];
			for (int i = 0; i < array.length; i++) {
				if(array[i] - 1 < array.length) {
					exist[array[i] - 1] = true;
				}
			}
			int i=0;
			while(i < exist.length && exist[i++] == true) {}
			
			if(i == exist.length ) {
				i++;
			}
			
			
			return i;
			
		}
		
	}
}

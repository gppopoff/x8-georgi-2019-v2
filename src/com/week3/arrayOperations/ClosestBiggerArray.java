package com.week3.arrayOperations;
 
public class ClosestBiggerArray {

	private boolean isValidIndex(int index, int[] array) {
		return index >= 0 && index < array.length;
	}

	public int[] closestBigger(int[] array) {
		int[] newArray = new int[array.length];

		for (int i = 0; i < array.length; i++) {
			boolean isFound = false;
			int positionOfBigger = i;
			for (int j = 1; !isFound && j < array.length; j++) {
				if (isValidIndex(i + j, array)) {
					if (array[i + j] > array[i]) {
						isFound = true;
						positionOfBigger = i + j;
					}
				}
				if (isValidIndex(i - j, array)) {
					if (array[i - j] > array[i]) {
						if (isFound) {
							if (array[i - j] > array[i + j]) {
								positionOfBigger = i - j;
							}
						} else {
							isFound = true;
							positionOfBigger = i - j;
						}
					}
				}
			}
			if (positionOfBigger != i) {
				newArray[i] = array[positionOfBigger];
			} else {
				newArray[i] = -1;
//				maxElementInArray = array[i];
			}
			isFound = false;

		}
		return newArray;
	}
}
package com.week3.arrayOperations;

public class SortedArray {
	
	int findMinMissing(int[] array,int n) {
		int expectedSum = (n * (n + 1)) / 2, sum = 0;
		for (int i = 0; i < array.length; i++) {
			sum = sum + array[i];
		}
		
		return expectedSum - sum;
	}
	
}

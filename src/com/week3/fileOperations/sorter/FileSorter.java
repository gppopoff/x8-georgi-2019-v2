package com.week3.fileOperations.sorter;

import java.io.File;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class FileSorter {
	
	void sortFile(String path) throws Exception {
		Path filePath = Paths.get(path);
		Path filePath1 = Paths.get("D:\\Jorko\\Workspace\\week-3-more-tasks\\temp1.txt");
		Path filePath2 = Paths.get("D:\\Jorko\\Workspace\\week-3-more-tasks\\temp2.txt");
		
		File File1 = new File("temp1.txt");
		File File2 = new File("temp2.txt");
		File File = new File(path);
		
				
		for (int i = 0; i < 31; i++) {
			
			Scanner scanner = new Scanner(filePath);
			PrintWriter pwriterF1 = new PrintWriter(File1);
			PrintWriter pwriterF2 = new PrintWriter(File2);
			
			while (scanner.hasNext()) {
				if (scanner.hasNextInt()) {
					
					int nextInt = scanner.nextInt();
					int mask = 1;
					mask <<= i;
					
					int neededBit = nextInt & mask;
					if( neededBit == 0 ) {
						pwriterF1.print(nextInt);
						pwriterF1.write(" ");
					}else {
						pwriterF2.print(nextInt);
						pwriterF2.write(" ");
					}
					
				} else {
					scanner.next();
				}
			}
			pwriterF2.close();
			pwriterF1.close();
			scanner.close();
			
			PrintWriter pwriter = new PrintWriter(File);
			Scanner scannerF1= new Scanner(filePath1);
			Scanner scannerF2 = new Scanner(filePath2);
			while(scannerF1.hasNext()) {
				if(scannerF1.hasNextInt()) {
					int nextInt = scannerF1.nextInt();
					pwriter.print(nextInt);
					pwriter.write(" ");
					
				} else {
					scannerF1.next();
				}
			}
			while(scannerF2.hasNext()) {
				if(scannerF2.hasNextInt()) {
					int nextInt = scannerF2.nextInt();
					pwriter.print(nextInt);
					pwriter.write(" ");
					
				} else {
					scannerF2.next();
				}
			}
			pwriter.close();
			scannerF1.close();
			scannerF2.close();
			
		}
		Scanner scanner = new Scanner(filePath);
		PrintWriter pwriterF1 = new PrintWriter(File1);
		PrintWriter pwriterF2 = new PrintWriter(File2);
		
		while (scanner.hasNext()) {
			if (scanner.hasNextInt()) {
				
				int nextInt = scanner.nextInt();
				
				int neededBit = nextInt >>> 31;
				if( neededBit == 1 ) {
					pwriterF1.print(nextInt);
					pwriterF1.write(" ");
				}else {
					pwriterF2.print(nextInt);
					pwriterF2.write(" ");
				}
				
			} else {
				scanner.next();
			}
		}
		pwriterF2.close();
		pwriterF1.close();
		scanner.close();
		
		PrintWriter pwriter = new PrintWriter(File);
		Scanner scannerF1= new Scanner(filePath1);
		Scanner scannerF2 = new Scanner(filePath2);
		while(scannerF1.hasNext()) {
			if(scannerF1.hasNextInt()) {
				int nextInt = scannerF1.nextInt();
				pwriter.print(nextInt);
				pwriter.write(" ");
				
			} else {
				scannerF1.next();
			}
		}
		while(scannerF2.hasNext()) {
			if(scannerF2.hasNextInt()) {
				int nextInt = scannerF2.nextInt();
				pwriter.print(nextInt);
				pwriter.write(" ");
				
			} else {
				scannerF2.next();
			}
		}
		pwriter.close();
		scannerF1.close();
		scannerF2.close();

		
	}
	
}

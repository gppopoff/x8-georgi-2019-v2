package com.week3.fileOperations.sorter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Scanner;

public class TextCompressor {
	boolean isLetter(char symbol) {
		return symbol >= 'a' && symbol <= 'z' || symbol >= 'A' && symbol <= 'Z';
	}

	void compress(File file) throws FileNotFoundException {
		HashMap<String, Integer> wordCounter = new HashMap<String, Integer>();
		PrintWriter pw = new PrintWriter("Encoder.txt");
		Scanner sc = new Scanner(file);
//		short counter = 0;
		while (sc.hasNextLine()) {
			StringBuilder line = new StringBuilder(sc.nextLine());
			String[] wordsFromLine = line.toString().split("[^a-zA-Z]+");
			String[] notWordsFromLine = line.toString().split("[a-zA-Z]+");
			for (int i = 0; i < wordsFromLine.length; i++) {
				if (!wordCounter.containsKey(wordsFromLine[i])) {
					wordCounter.put(wordsFromLine[i], 1);
				} else {
					wordCounter.replace(wordsFromLine[i], wordCounter.get(wordsFromLine[i]) + 1);
				}
			}

		}

		wordCounter.entrySet().forEach(entry -> {
			System.out.println(entry.getValue());
			if(entry.getValue() >= 3) {
				pw.print(entry.getKey() + " " + entry.getValue());
				pw.println();
			}
		});
		sc.close();
		pw.close();
	}
}

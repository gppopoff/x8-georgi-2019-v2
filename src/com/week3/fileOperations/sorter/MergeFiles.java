package com.week3.fileOperations.sorter;

import java.io.File;
import java.io.PrintWriter;
import java.util.Scanner;

public class MergeFiles {

	public File mergeWithSort(String file1Path, String file2Path) throws Exception {
		FileSorter sorter = new FileSorter();
		sorter.sortFile(file1Path);
		sorter.sortFile(file2Path);

		File newFile = new File("NewSortedFile.txt");
		File one = new File(file1Path);
		File two = new File(file2Path);

		PrintWriter pw = new PrintWriter(newFile);
		Scanner sc1 = new Scanner(one);
		Scanner sc2 = new Scanner(two);

		int numOne = Integer.parseInt(sc1.next());
		int numTwo = Integer.parseInt(sc2.next());

		while (sc1.hasNext() && sc2.hasNext()) {

			if (numOne < numTwo) {
				pw.print(numOne);
				pw.write(" ");
				while (!sc1.hasNextInt()) {
					sc1.next();
				}
				numOne = sc1.nextInt();
			} else {
				pw.print(numTwo);
				pw.write(" ");
				while (!sc2.hasNextInt()) {
					sc2.next();
				}
				numTwo = sc2.nextInt();
			}
//			if (sc1.hasNextInt()) {
//				if (sc2.hasNextInt()) {
//					numOne = sc1.nextInt();
//					numTwo = sc2.nextInt();
//
//					if (numOne > numTwo) {
//
//					}
//
//				} else {
//					sc2.next();
//				}
//			} else {
//				sc1.next();
//			}
		}
		if (numOne > numTwo) {
			pw.print(numTwo);
			pw.write(" ");
			pw.print(numOne);
			pw.write(" ");
		} else {
			pw.print(numOne);
			pw.write(" ");
			pw.print(numTwo);
			pw.write(" ");
		
		}
		while (sc1.hasNext()) {
			if (sc1.hasNextInt()) {
				pw.print(sc1.nextInt());
				pw.write(" ");
			} else {
				sc1.next();
			}
		}
		while (sc2.hasNext()) {
			if (sc2.hasNextInt()) {
				pw.print(sc2.nextInt());
				pw.write(" ");
			} else {
				sc2.next();
			}
		}
		pw.close();
		sc1.close();
		sc2.close();

		return newFile;
	}

}

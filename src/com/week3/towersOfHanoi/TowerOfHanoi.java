package com.week3.towersOfHanoi;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class TowerOfHanoi {
	
	private List< Stack<Integer> > towers = new ArrayList< Stack<Integer> >(4);
	
	
	private void fill(int Elements) {
		this.towers.add(new Stack<Integer>());
		this.towers.add(new Stack<Integer>());
		this.towers.add(new Stack<Integer>());
		this.towers.add(new Stack<Integer>());
		
		
		for(int i = Elements ; i > 0 ; i-- ) {
			this.towers.get(1).push(i);
		}
	}
	
	public void MoveTower(int Elements) {
		fill(Elements);
		for (int i = 1; i < 4; i++) {
			if(!this.towers.get(i).empty()) {
				System.out.println(this.towers.get(i).toString());
			}else {
				System.out.println("[ ]");
			}
			
		}
		System.out.println("------------------------");
		moveTowerTo(Elements,1,2,3);
	}
	
	private void moveTowerTo(int numOfElements, int from, int to, int helper) {
		
		if(numOfElements > 0) {
			moveTowerTo(numOfElements - 1, from, helper, to);
			
			
			
			this.towers.get(to).push(this.towers.get(from).pop());
			for (int i = 1; i < 4; i++) {
				if(!this.towers.get(i).empty()) {
					System.out.println(this.towers.get(i).toString());
				}else {
					System.out.println("[ ]");
				}
				
			}
			
			System.out.println("------------------------");
			moveTowerTo(numOfElements - 1, helper, to, from);
			
			
		}
			
	}
}
 
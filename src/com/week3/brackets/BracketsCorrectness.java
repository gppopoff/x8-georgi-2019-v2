package com.week3.brackets;

import java.util.Stack;

public class BracketsCorrectness {

	private boolean isStartingBracket(char symbol) {
		return symbol == '(' || symbol == '{' || symbol == '[';
	}
	
	private boolean isClosingBracket(char symbol) {
		return symbol == ')' || symbol == '}' || symbol == ']';
	}
	
	private boolean isSameBracket(char first, char second) {
		if(first == '[' && second == ']' || first == '(' && second == ')' || first == '{' && second == '}') {
			return true;
		}
		return false;
	}
	
	boolean isBracketsCorrect (String expr) {
		
		Stack<Character> stack = new Stack<Character>();
		char[] exprInChar = expr.toCharArray();
		
		for (int i = 0; i < exprInChar.length; i++){
			if(isStartingBracket(exprInChar[i])) {
				stack.push(exprInChar[i]);
			}
			if(isClosingBracket(exprInChar[i])) {
				if(isSameBracket(stack.peek(), exprInChar[i])) {
					stack.pop();
				}else {
					return false;
				}
			}
		}
		if(!stack.empty()) {
			return false;
		}
		return true;
	}
}

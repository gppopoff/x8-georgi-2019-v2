package com.week3.numberConventor;

import java.util.Stack;

public class DecToBin {
	public String ToBin (int i) {
		Stack<Integer> stack = new Stack<Integer>();
		if( i == 0) {
			return "0";
		}
		while(i > 0) {
			stack.push(i%2);
			i/=2;
		}
		
		StringBuilder binary = new StringBuilder();
		while(!stack.empty()) {
			binary.append(stack.pop());
		}
		
		return binary.toString();
	}
	
}

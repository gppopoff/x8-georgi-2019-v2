package com.filesystem.files;

public class FSLine {
	private int index;
	private String content;
	private int numOfChars;

	public FSLine() {
		index = 0;
		content = new String();
		numOfChars = 0;
	}

	public FSLine(int newIndex, String newContent) {
		index = newIndex;
		content = new String(newContent);
		numOfChars = content.length();
	}

	public void setIndex(int newIndex) {
		index = newIndex;
	}

	public void setContent(String newContent) {
		content = new String(newContent);
		numOfChars = content.length();
	}

	public int getIndex() {
		return index;
	}

	public String getContent() {
		return content;
	}

	public int getNumOfChars() {
		return numOfChars;
	}

	public void addContent(String strToAdd) {
		content += strToAdd;
		numOfChars += strToAdd.length();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((content == null) ? 0 : content.hashCode());
		result = prime * result + index;
		result = prime * result + numOfChars;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FSLine other = (FSLine) obj;
		if (content == null) {
			if (other.content != null)
				return false;
		} else if (!content.equals(other.content))
			return false;
		if (index != other.index)
			return false;
		if (numOfChars != other.numOfChars)
			return false;
		return true;
	}
}

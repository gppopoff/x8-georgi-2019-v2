package com.filesystem.files;

import java.util.*;
import java.util.Map.Entry;

public class FSTextFile 
extends FSObject
implements FSFile {
	private Map<Integer, FSLine> lines;
	private int size;
	private int lastLineIndex;

	public FSTextFile(String name) {
		super(name);
		lines = new HashMap<Integer, FSLine>();
		size = 0;
		lastLineIndex = 0;
	}

	public void addLine(FSLine newLine) {
		if (newLine.getIndex() < 1) {
			System.out.println("Incorect index for line. Try again!");
		} else {
			if (lines.containsKey(newLine.getIndex())) {
				lines.get(newLine.getIndex()).addContent(newLine.getContent().toString());
				size += newLine.getNumOfChars();
			} else {
				lines.put(newLine.getIndex(), newLine);
				size += newLine.getNumOfChars();
				if (newLine.getIndex() > lastLineIndex) {
					size += newLine.getIndex() - lastLineIndex;
					lastLineIndex = newLine.getIndex();
				}
			}
		}
	}

	public void removeLine(int index) {
		if (index < 1) {
			System.out.println("Invalid index!");
		} else {
			if (lines.containsKey(index)) {
				if (lastLineIndex == index) {
//					Integer max = 0;
//					lines.entrySet().forEach(entry -> {
//						if(entry.getKey() > max) max = entry.getKey();
//					});
					int tempNumOfCharsOfIndex = lines.get(index).getNumOfChars();
					lines.remove(index);
					Optional<Entry<Integer, FSLine>> lastLine = lines.entrySet().stream()
							.reduce((line1, line2) -> line1.getKey() > line2.getKey() ? line1 : line2);
					size -= (index) + tempNumOfCharsOfIndex;
					if (lastLine.isPresent()) {
						size += lastLine.get().getKey();
						lastLineIndex = lastLine.get().getKey();
					} else {
						lastLineIndex = 0;
					}
//					Integer max = 0;
//					for (Integer lineIndex : lines.keySet()) {
//						if (max < lineIndex && lineIndex != index) {
//							max = lineIndex;
//						}
//					}
//					size -= (index - max) + lines.get(index).getNumOfChars();
				} else {
					size -= lines.get(index).getNumOfChars();
				}
//				lines.remove(index);
			} else {
				System.out.println("Nothing to remove!");
			}
		}
	}

	public FSLine getLine(int index) {
		return lines.get(index);
	}

	public Map<Integer, FSLine> getAllMaps() {
		return lines;
	}

	public int getSize() {
		return size;
	}

	public int getLastLineIndex() {
		return lastLineIndex;
	}

	public void printLine(int index) {
		System.out.println(lines.get(index).getContent());
	}

	public String show() {
		StringBuilder result = new StringBuilder();
		for (int i = 1; i <= lastLineIndex; i++) {
			if (lines.containsKey(i)) {
				result.append(lines.get(i).getContent());
				result.append("\n");
			} else {
				result.append("\n");
			}
		}
		return result.toString();
	}

	public void printInfo() {
		System.out.println(this.getName() + " -tf" + " Size:" + this.getSize());
	}
}
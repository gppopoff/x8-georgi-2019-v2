package com.filesystem.files;

public interface FSFile {

	public String show();

	public int getSize();

	public void printInfo();
}

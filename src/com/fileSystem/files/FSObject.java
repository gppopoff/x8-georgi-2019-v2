package com.filesystem.files;

public abstract class FSObject implements FSFile {
	private String name;

	public FSObject(String newName) {
		super();
		name = newName;
	}

	public void setName(String newName) {
		name = newName;
	}

	public String getName() {
		return name;
	}
}

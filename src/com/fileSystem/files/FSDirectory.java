package com.filesystem.files;

import java.util.*;

public class FSDirectory extends FSObject implements FSFile {
	private FSDirectory parent;
	private Map<String, FSObject> objects;
	private Set<String> setOfNames;

	public FSDirectory(String newName) {
		super(newName);
		parent = null;
		objects = new HashMap<String, FSObject>();
		setOfNames = new TreeSet<String>();
	}

	public FSDirectory(String newName, FSDirectory parent) {
		super(newName);
		this.parent = parent;
		objects = new HashMap<String, FSObject>();
		setOfNames = new TreeSet<String>();
	}

	public void setParent(FSDirectory parent) {
		this.parent = parent;
	}

	public FSDirectory getParent() {
		return parent;
	}

	public void addObject(FSObject object) {
		if (containsName(object.getName())) {
			System.out.println("Name is already taken!");
		} else {
			objects.put(object.getName(), object);
			setOfNames.add(object.getName());
		}
	}

	public void removeObject(String objectName) {
		if (objects.containsKey(objectName)) {
			objects.remove(objectName);
			setOfNames.remove(objectName);
		} else {
			System.out.println("No object with such name");
		}
	}

	public boolean containsName(String name) {
		return objects.containsKey(name);
	}

	// Moje bi funkciq koqto vryshta celiq map ??
	public FSObject getObjectByKey(String key) {
		return objects.get(key);
	}

	public Map<String, FSObject> getObjects() {
		return objects;
	}

	public String show() {
		StringBuilder result = new StringBuilder();
		for (String names : setOfNames) {
			result.append(names + " ");
		}
		return result.toString();
//		for (FSObject object : objects.values()) {
//			object.printInfo();
//		}
	}

	public int getSize() {
		int size = 0;
		for (FSObject object : objects.values()) {
			size += object.getSize();
		}
		return size;
	}

	public void printInfo() {
		System.out.println(this.getName() + " -d" + " Size:" + this.getSize());
	}

	public String showSorted() {
		StringBuilder result = new StringBuilder();
		Collection<FSObject> sorted = objects.values();
		sorted.stream().sorted((e1, e2) -> {
			return e2.getSize() - e1.getSize();
		}).forEach((fs) -> {			
			fs.printInfo();
			result.append(fs.getName());
			result.append(" ");
		});
		return result.toString();
	}
}

package com.filesystem.comands;

import com.filesystem.files.FSDirectory;

public class ChangeDir extends Command {

	public ChangeDir(FSDirectory dir) {
		super(dir);
	}

	@Override
	public void execute(String[] arg) {
		if (arg.length > 1) {
			FSDirectory tempDirPointer = currentDir;
			boolean problemOccurred = false;
			String[] path = arg[1].split("/");
			if (path[0].equals("")) {
				while (!tempDirPointer.getName().equals("/")) {
					tempDirPointer = tempDirPointer.getParent();
				}
				for (int i = 1; i < path.length; i++) {
					if (tempDirPointer.containsName(path[i]) 
							&& tempDirPointer.getObjectByKey(path[i]).getClass() == FSDirectory.class) {
						tempDirPointer = (FSDirectory)tempDirPointer.getObjectByKey(path[i]);
					} else {
						problemOccurred = true;
					}
				}
			} else {
				for (String command : path) {
					if (command.equals("..")) {
						if (!tempDirPointer.getName().equals("/")) {
							tempDirPointer = tempDirPointer.getParent();
						}
					} else if (tempDirPointer.containsName(command) 
							&& tempDirPointer.getObjectByKey(command).getClass() == FSDirectory.class) {
						tempDirPointer = (FSDirectory)tempDirPointer.getObjectByKey(command);
					} else if (command.equals(".")) {} else {
						problemOccurred = true;
					}
				}
			}
			if(!problemOccurred) {
				currentDir = tempDirPointer;
			} else {
				System.out.println("Wrong Path or ...");
			}
			
//			if (arg[1].equals("..") && !currentDir.getName().equals("/")) {
//				currentDir = currentDir.getParent();
//			} else {
//				if (currentDir.containsName(arg[1])
//						&& currentDir.getObjectByKey(arg[1]).getClass() == FSDirectory.class) {
//					currentDir = (FSDirectory) currentDir.getObjectByKey(arg[1]);
//				} else {
//					System.out.println("Invalid command!");
//				}
//			}
		}
	}

}

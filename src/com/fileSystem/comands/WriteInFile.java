package com.filesystem.comands;

import java.util.ArrayList;
import java.util.List;

import com.filesystem.files.FSDirectory;
import com.filesystem.files.FSLine;
import com.filesystem.files.FSTextFile;

public class WriteInFile extends Command {

	public WriteInFile(FSDirectory dir) {
		super(dir);
	}

	private int overWrite(String[] arg) {
		for (int i = 1; i < arg.length; i++) {
			if(arg[i].equals("-overwrite")) {
				return i;
			}
		}
		return 0;
	}
	@Override
	public void execute(String[] arg) {
		int overWritePosition = overWrite(arg);
		if (overWritePosition!= 0 && arg.length > 4) {
			List<String> newArgs = new ArrayList<String>();
			for (int i = 0; i < arg.length; i++) {
				if (i != overWritePosition) {
					newArgs.add(arg[i]);
				}
			}
			System.out.println(newArgs.toString());
			if (currentDir.containsName(newArgs.get(1))
				&& currentDir.getObjectByKey(newArgs.get(1)).getClass() == FSTextFile.class) {
				FSTextFile newOne = (FSTextFile) currentDir.getObjectByKey(newArgs.get(1));
				newOne.removeLine(Integer.parseInt(newArgs.get(2)));
				newOne.addLine(new FSLine(Integer.parseInt(newArgs.get(2)), newArgs.get(3)));
			}
		} else if (arg.length > 3 && currentDir.containsName(arg[1])
				&& currentDir.getObjectByKey(arg[1]).getClass() == FSTextFile.class) {
			FSTextFile newOne = (FSTextFile) currentDir.getObjectByKey(arg[1]);
			newOne.addLine(new FSLine(Integer.parseInt(arg[2]), arg[3]));
		} else {
			
		}
	}

}

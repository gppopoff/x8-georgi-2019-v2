package com.filesystem.comands;

import com.filesystem.files.FSDirectory;

public class MakeDir extends Command {

	public MakeDir(FSDirectory dir) {
		super(dir);
	}

	@Override
	public void execute(String[] arg) {
		if (arg.length > 1 && arg[1].length() > 0) {
			currentDir.addObject(new FSDirectory(arg[1], currentDir));
		}
	}
}

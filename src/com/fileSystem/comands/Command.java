package com.filesystem.comands;

import com.filesystem.files.FSDirectory;

public abstract class Command {
	static protected FSDirectory currentDir; //= new FSDirectory("");
	protected static void setDir (FSDirectory dir) {
		currentDir = dir;
	}
	public static FSDirectory getCurrentDir() {
		return currentDir;
	}
	public Command(FSDirectory dir) {
		currentDir = dir;
	}
	public abstract void execute(String[] arg);
}

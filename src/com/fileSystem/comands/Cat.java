package com.filesystem.comands;

import com.filesystem.files.FSDirectory;
import com.filesystem.files.FSTextFile;

public class Cat extends Command {

	public Cat(FSDirectory dir) {
		super(dir);
	}

	@Override
	public void execute(String[] arg) {
		if (arg.length > 1 && currentDir.containsName(arg[1])
				&& currentDir.getObjectByKey(arg[1]).getClass() == FSTextFile.class) {
			System.out.println(currentDir.getObjectByKey(arg[1]).show());
		}
	}

}

package com.filesystem.comands;

import com.filesystem.files.FSDirectory;
import com.filesystem.files.FSTextFile;

public class CreateFile extends Command {

	public CreateFile(FSDirectory dir) {
		super(dir);
	}

	@Override
	public void execute(String[] arg) {
		if (arg.length > 1 && arg[1].length() > 0) {
			currentDir.addObject(new FSTextFile(arg[1]));
		}
	}
}

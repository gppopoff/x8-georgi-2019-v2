package com.filesystem.comands;

import com.filesystem.files.FSDirectory;

public class ListContent extends Command {

	public ListContent(FSDirectory dir) {
		super(dir);
	}

	@Override
	public void execute(String[] arg) {
		if (arg.length == 1) {
			System.out.println(currentDir.show());
			// TO DO return .show(); for pipe
		} else {
			if (arg.length > 1 && arg[1].equals("--sorted")) {
				// TO DO ...
				System.out.println(currentDir.showSorted());
			}
		}
	}

}

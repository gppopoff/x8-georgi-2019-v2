package com.filesystem;

import com.filesystem.files.FSDirectory;

public class FileSystem {
	FSDirectory root;
	FSDirectory home;
	
	public FileSystem() {
		root = new FSDirectory("/");
		home = new FSDirectory("home", root);
		root.addObject(home);
	}
	
	public void openTerminal() {
		Terminal cmd = new Terminal(home);
		cmd.run();
	}
}

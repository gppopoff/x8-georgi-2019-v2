package com.filesystem;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import com.filesystem.comands.*;

import com.filesystem.files.FSDirectory;
import com.filesystem.files.FSTextFile;

public class Terminal {
	private FSDirectory currentDir;
	static private Map<String, Command> commands = new HashMap<>();
	public Terminal(FSDirectory startDir) {
		currentDir = startDir;
		commands.put("mkdir", new MakeDir(currentDir));
		commands.put("create_file", new CreateFile(currentDir));
		commands.put("cat", new Cat(currentDir));
		commands.put("cd", new ChangeDir(currentDir));
		commands.put("write", new WriteInFile(currentDir));
		commands.put("ls", new ListContent(currentDir));
	}

	public void run() { 
		Scanner input = new Scanner(System.in);
		String[] line = input.nextLine().split(" ");

		while(!line[0].equals("exit")) {
			if(commands.containsKey(line[0])) {
				commands.get(line[0]).execute(line);
				currentDir = Command.getCurrentDir();
			} else {
				System.out.println("No such command!");
			}
			line = input.nextLine().split(" ");
		}
	}
}

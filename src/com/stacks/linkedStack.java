package com.stacks;

import java.util.EmptyStackException;

import com.lists.*;

public class linkedStack {
	private Listnode items;
	int numItems;
	
	public linkedStack() {
		numItems = 0;
		items = null;
	}
	
	public boolean empty() {
		if(numItems == 0) {
			return true;
		}else {
			return false;
		}
	}
	
	public int size() {
		return numItems;
	}
	
	public void push(Object item) {
		items = new Listnode(item, items);
		numItems++;
	}
	
	public void pop() throws EmptyStackException{
		if(empty()) {
			throw new EmptyStackException();
		}else {
			items = items.getNext();
			numItems--;
		}	
	}
	
	public Object top() throws EmptyStackException {
		if(empty()) {
			throw new EmptyStackException();
		}else {
			return items.getData();
		}
	}
}

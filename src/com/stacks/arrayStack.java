package com.stacks;

import java.util.EmptyStackException;

public class arrayStack {
	private Object[] items;
	private int numItems;
	private int currentMax;
	
	public arrayStack() {
		currentMax = 4;
		items = new Object[currentMax];
		numItems = 0;
	}
	
	private void expandStack() {
		Object[] temp = items;
		currentMax *= 2;
		items = new Object[currentMax];
		for(int i = 0; i < numItems; i++) {
			items[i] = temp[i];
		}
		temp = null;
	}
	
	public void push(Object newItem) {
		if(numItems == currentMax) {
			expandStack();
		}
		items[numItems] = newItem;
		numItems++;
	}
	
	public int size() {
		return numItems;
	}
	
	public boolean empty() {
		if(numItems == 0) {
			return true;
		}else {
			return false;
		}
	}
	
	public void pop() throws EmptyStackException {
		if(empty()) {
			throw new EmptyStackException();
		} else {
			numItems--;
		}
	}
	
	public Object top() throws EmptyStackException{
		if(empty()) {
			throw new EmptyStackException();
		}
		return items[numItems - 1];
	}
}

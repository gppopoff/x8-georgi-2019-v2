package com.week3.numberConventor;

import static org.junit.Assert.*;

import org.junit.Test;

import com.week3.numberConventor.DecToBin;

public class DecToBinTest {

	@Test
	public void test_WithNormalNumbers_ExpectEquals() {
		DecToBin convector = new DecToBin();
		assertEquals("0",convector.ToBin(0));
		assertEquals("1",convector.ToBin(1));
		assertEquals("10",convector.ToBin(2));
		assertEquals("1010",convector.ToBin(10));
		assertEquals("1011",convector.ToBin(11));
		assertEquals("1100",convector.ToBin(12));
		assertEquals("1111100",convector.ToBin(124));
		
	}

}

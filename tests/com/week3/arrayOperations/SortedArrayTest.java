package com.week3.arrayOperations;

import static org.junit.Assert.*;

import org.junit.Test;

import com.week3.arrayOperations.SortedArray;

public class SortedArrayTest {

	@Test
	public void test() {
		SortedArray tester = new SortedArray();
		assertEquals(4, tester.findMinMissing(new int[] {1,2,3,5,6}, 6) );
		assertEquals(1, tester.findMinMissing(new int[] {2,3,4,5,6}, 6) );
		assertEquals(6, tester.findMinMissing(new int[] {1,2,3,4,5}, 6) );
		assertEquals(2, tester.findMinMissing(new int[] {1,3,4,5,6}, 6) );
		assertEquals(3, tester.findMinMissing(new int[] {1,2,4,5,6}, 6) );
		
	}

}

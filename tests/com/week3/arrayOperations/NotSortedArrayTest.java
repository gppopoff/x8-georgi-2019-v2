package com.week3.arrayOperations;

import static org.junit.Assert.*;

import org.junit.Test;

import com.week3.arrayOperations.NotSortedArray;

public class NotSortedArrayTest {

	@Test
	public void test_WithNoMissingInArray_ExpectEquals() {
		NotSortedArray tester = new NotSortedArray();
		int[] a = {1,5,6,7,4,2,3};
		assertEquals(8 ,tester.findMinMissing(a, 7));
	}
	
	@Test
	public void test_WithOneMissingInTheMiddle_ExpectEquals() {
		NotSortedArray tester = new NotSortedArray();
		int[] a = {1,8,6,7,4,2,3};
		assertEquals(5 ,tester.findMinMissing(a, 8));
	}
	
	@Test
	public void test_WithOneMissingInTheBeginning_ExpectEquals() {
		NotSortedArray tester = new NotSortedArray();
		int[] a = {5,8,6,7,4,2,3};
		assertEquals(1 ,tester.findMinMissing(a, 8));
	}
	
	@Test
	public void test_WithMoreMissingInTheBeginning_ExpectEquals() {
		NotSortedArray tester = new NotSortedArray();
		int[] a = {3,5,6,4,7};
		assertEquals(1 ,tester.findMinMissing(a, 7));
	}
	
	@Test
	public void test_WithMoreMissingInTheBeginning_ExpectEquals123() {
		NotSortedArray tester = new NotSortedArray();
		int[] a = {1,2,3,4};
		assertEquals(5 ,tester.findMinMissing(a, 7));
	}
	
	@Test
	public void test_WithMoreMissingInTheBeginning_ExpectEquals13() {
		NotSortedArray tester = new NotSortedArray();
		int[] a = {1,2};
		assertEquals(3 ,tester.findMinMissing(a, 5));
	}
	
	

}

package com.week3.arrayOperations;

import static org.junit.Assert.*;

import org.junit.Test;

import com.week3.arrayOperations.ClosestBiggerArray;

public class ClosestBiggerArrayTest {

	@Test
	public void test() {
		ClosestBiggerArray tester = new ClosestBiggerArray();
		
		assertArrayEquals(new int[] {8, 8, 11, 11, 12, 20, -1, 20} , tester.closestBigger(new int[] {4, 3, 8, 5, 11, 12, 20, 1}) );
		
		assertArrayEquals(new int[] {2, 3, 10, -1, 10, 10} , tester.closestBigger(new int[] {1, 2, 3, 10, 4, 5}) );
		
		assertArrayEquals(new int[] {2, 3, 3, 3, -1, 3, 2, 2, 3, -1} , tester.closestBigger(new int[] {1, 2, 2, 1, 3, 2, 1, 1, 2, 3}) );
	}

}

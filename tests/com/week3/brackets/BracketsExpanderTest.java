package com.week3.brackets;

import static org.junit.Assert.*;

import org.junit.Test;

import com.week3.brackets.SimpleBracketsExpander;

public class BracketsExpanderTest {

	@Test
	public void test_ExpandWithString_ExpectEquals() {
		SimpleBracketsExpander tester = new SimpleBracketsExpander();
		
		assertEquals("ABDCDCDCFF" , tester.expand("AB3(DC)2(F)"));
		assertEquals("ABDCDCDCFFF" , tester.expand("AB3(DC)3(F)"));
		assertEquals("DCDCDCFF" , tester.expand("3(DC)2(F)"));
		assertEquals("DCDCDC" , tester.expand("3(DC)"));
		assertEquals("ABDCF" , tester.expand("ABDCF"));
	}

}

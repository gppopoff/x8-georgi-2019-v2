package com.week1tasks;

import static org.junit.Assert.*;

import org.junit.Test;

public class vectorFuncsTest {

	@Test
	public void testScalarSum_WithBasicVectors() {
		vectorFuncs vectorTester = new vectorFuncs();
		int[] first = {1,1,1,1,1};
		int[] second = {1,1,1,1,1};
		assertEquals(5, vectorTester.scalarSum(first, second));
		
	}

	@Test
	public void testScalarSum_WithNullVectors() {
		vectorFuncs vectorTester = new vectorFuncs();
		int[] first = {0,0,0,0,0};
		int[] second = {1,1,1,1,1};
		assertEquals(0, vectorTester.scalarSum(first, second));
		
	}
	
	@Test
	public void testMaxScalarSum_WithOneUnsortedArray() {
		vectorFuncs vectorTester = new vectorFuncs();
		int[] first = {5, 4, 1, 2, 3};
		int[] second = {1, 1, 1, 1, 1};
		assertEquals(15, vectorTester.maxScalarSum(first, second));
		
	}

	@Test
	public void testMaxScalarSum_WithTwoUnsortedArrays() {
		vectorFuncs vectorTester = new vectorFuncs();
		int[] first = {5, 4, 1, 2, 3};
		int[] second = {3, 1, 4, 5, 2};
		assertEquals(55, vectorTester.maxScalarSum(first, second));
		
	}
	
	@Test
	public void testMaxScalarSum_WithNullArray() {
		vectorFuncs vectorTester = new vectorFuncs();
		int[] first = {5, 4, 1, 2, 3};
		int[] second = {0, 0, 0, 0, 0};
		assertEquals(0, vectorTester.maxScalarSum(first, second));
		
	}
}

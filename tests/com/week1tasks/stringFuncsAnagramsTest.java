package com.week1tasks;

import static org.junit.Assert.*;

import org.junit.Test;

public class stringFuncsAnagramsTest {

	@Test
	public void testIsAnagramWithAnagrams() {
		stringFuncs strTester = new stringFuncs();
		assertTrue(strTester.isAnagram("", ""));
		assertTrue(strTester.isAnagram("Abba", "babA"));
		assertTrue(strTester.isAnagram("A1aa", "a!a!A"));
		assertTrue(strTester.isAnagram("Aaa", "!aaA"));
	}
	
	@Test
	public void testIsAnagramWithNotAnagrams() {
		stringFuncs strTester = new stringFuncs();
		assertFalse(strTester.isAnagram("", "A"));
		assertFalse(strTester.isAnagram("A", ""));
		assertFalse(strTester.isAnagram("Aaaa", "Aaa"));
		assertFalse(strTester.isAnagram("Aaa", "Aaaa"));
		
	}
	
	@Test
	public void testHasAnagramOfTrue() {
		stringFuncs strTester = new stringFuncs();
		assertTrue(strTester.hasAnagramOf("AabaB", "aba"));
		assertTrue(strTester.hasAnagramOf("abaB", "aba"));
		assertTrue(strTester.hasAnagramOf("Aaba", "aba"));
		assertTrue(strTester.hasAnagramOf("Aa!baB", "aba"));
		assertTrue(strTester.hasAnagramOf("AabaB", "a!ba"));

		assertTrue(strTester.hasAnagramOf("", ""));
		assertTrue(strTester.hasAnagramOf("abbaB", ""));
	}
	
	@Test
	public void testHasAnagramOfFalse() {
		stringFuncs strTester = new stringFuncs();
		assertFalse(strTester.hasAnagramOf("", "aba"));
		assertFalse(strTester.hasAnagramOf("AAAA", "aa"));
		assertFalse(strTester.hasAnagramOf("aa", "AAAA"));
	}
}

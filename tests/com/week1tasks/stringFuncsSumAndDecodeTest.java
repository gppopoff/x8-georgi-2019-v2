package com.week1tasks;

import static org.junit.Assert.*;

import org.junit.Test;

public class stringFuncsSumAndDecodeTest {

	@Test
	public void testDecodeUrl() {
		stringFuncs strTester = new stringFuncs();
		assertEquals("",strTester.decodeUrl(""));
		assertEquals("aaa aaa:aa?aaa/aaa",strTester.decodeUrl("aaa%20aaa%3Aaa%3Daaa%2Faaa"));
		assertEquals("::",strTester.decodeUrl("%3A%3A"));
		assertEquals("::aaa",strTester.decodeUrl("%3A%3Aaaa"));
		assertEquals("aaa::",strTester.decodeUrl("aaa%3A%3A"));
		assertEquals("::aaa::",strTester.decodeUrl("%3A%3Aaaa%3A%3A"));
	}

	@Test
	public void testSumOfNumbers() {
		stringFuncs strTester = new stringFuncs();
		assertEquals( 0, strTester.sumOfNumbers(""));
		assertEquals( 444, strTester.sumOfNumbers("123aa321"));
		assertEquals( 444, strTester.sumOfNumbers("asd123aaa321aaa"));
		assertEquals( 123, strTester.sumOfNumbers("123"));
		assertEquals( 6, strTester.sumOfNumbers("1 2 3"));
	}

}

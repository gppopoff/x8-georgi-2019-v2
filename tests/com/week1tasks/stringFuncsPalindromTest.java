package com.week1tasks;

import static org.junit.Assert.*;

import org.junit.Test;

public class stringFuncsPalindromTest {

	@Test
	public void testIsPalindromeStringTrue() {
		stringFuncs strTester = new stringFuncs();
		assertTrue(strTester.isPalindrome(""));
		assertTrue(strTester.isPalindrome("b"));
		assertTrue(strTester.isPalindrome("aBa"));
		assertTrue(strTester.isPalindrome("!!!"));
		assertTrue(strTester.isPalindrome("aBBa"));
		assertTrue(strTester.isPalindrome("aB Ba"));
		assertTrue(strTester.isPalindrome("aaBB  BBaa"));
		assertTrue(strTester.isPalindrome("aB ! Ba"));
		
	}
	@Test
	public void testIsPalindromeStringFalse() {
		stringFuncs strTester = new stringFuncs();
		assertFalse(strTester.isPalindrome("aB"));
		assertFalse(strTester.isPalindrome("aBaB"));
		assertFalse(strTester.isPalindrome("!! !"));
		assertFalse(strTester.isPalindrome(" aBBa"));
		assertFalse(strTester.isPalindrome("aB BBa"));
		assertFalse(strTester.isPalindrome("aaB  BBaa"));
		assertFalse(strTester.isPalindrome("aB ! a"));
		
	}
	@Test
	public void testIsPalindromeInt() {
		stringFuncs strTester = new stringFuncs();
		assertTrue(strTester.isPalindrome(0));
		assertTrue(strTester.isPalindrome(11));
		assertTrue(strTester.isPalindrome(121));
		assertTrue(strTester.isPalindrome(1221));
		
		assertFalse(strTester.isPalindrome(12));
		assertFalse(strTester.isPalindrome(122));
		assertFalse(strTester.isPalindrome(1212));
		
	}

	@Test
	public void testGetLargestPalindromeExact() {
		stringFuncs strTester = new stringFuncs();
		assertEquals(0 ,strTester.getLargestPalindrome(0));
		assertEquals(11 ,strTester.getLargestPalindrome(11));
		assertEquals(121 ,strTester.getLargestPalindrome(121));
		assertEquals(111 ,strTester.getLargestPalindrome(111));
		assertEquals(2222 ,strTester.getLargestPalindrome(2222));
	}
	@Test
	public void testGetLargestPalindromeNear() {
		stringFuncs strTester = new stringFuncs();
		assertEquals(11 ,strTester.getLargestPalindrome(12));
		assertEquals(99 ,strTester.getLargestPalindrome(100));
		assertEquals(111 ,strTester.getLargestPalindrome(120));
		assertEquals(191 ,strTester.getLargestPalindrome(199));
		assertEquals(2112 ,strTester.getLargestPalindrome(2220));
	}

}

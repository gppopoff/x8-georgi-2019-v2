package com.week1tasks;

import static org.junit.Assert.*;

import org.junit.*;

import com.week1tasks.exception.*;

public class arrayChecksTest {

	@Test
	public void testMinElement() {
		arrayChecks arrTests = new arrayChecks();
		assertEquals(0, arrTests.minElement());
		assertEquals(0, arrTests.minElement(1,2,8,4,0,5));
		assertEquals(-10, arrTests.minElement(1,15,0,-10,-10,0,1,0));
		assertEquals(1, arrTests.minElement(1,2,3,4,5,6));
		assertEquals(1, arrTests.minElement(9,5,4,7,2,1));
	}

	@Test
	public void testKthMin() {
		arrayChecks arrTests = new arrayChecks();
		
		assertEquals(0,arrTests.kthMin(1,new int[] {1,1,2,34,45,0,10}));
		assertEquals(1,arrTests.kthMin(2,new int[] {1,1,2,34,45,0,10}));
	}

	@Test
	public void testGetOddOccurrence() {
		arrayChecks arrTests = new arrayChecks();
		
		try {
			assertEquals(0,arrTests.getOddOccurrence());
		} catch (EmptyArrayExeption e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			System.out.println("Glupav si");
		}
		try {
			assertEquals(1,arrTests.getOddOccurrence(8,2,3,4,3,2,4,8,1,2,2));
		} catch (EmptyArrayExeption e) {
			e.printStackTrace();
		}
		try {
			assertEquals(1,arrTests.getOddOccurrence(8,2,3,4,3,2,4,8,1,2));
		} catch (EmptyArrayExeption e) {
			e.printStackTrace();
		}
		
	}

	@Test
	public void testGetAverage() {
		arrayChecks arrTests = new arrayChecks();
		assertEquals(4 ,arrTests.getAverage(new int[] {1,2,3,4,5,6,7}));
		assertEquals(7 ,arrTests.getAverage(new int[] {1,2,3,4,5,6,28}));
		assertNotEquals(4 ,arrTests.getAverage(new int[] {1,2,3,4,5,6}));
	}

	@Test
	public void testMaxSpan() {
		arrayChecks arrTests = new arrayChecks();
		assertEquals(4 ,arrTests.maxSpan(new int[] {2, 5, 4, 1, 3, 4}));
		assertEquals(6 ,arrTests.maxSpan(new int[] {8, 12, 7, 1, 7, 2, 12}));
		assertEquals(6 ,arrTests.maxSpan(new int[] {3, 6, 6, 8, 4, 3, 6}));
		assertEquals(1 ,arrTests.maxSpan(new int[] {3, 6, 8, 7, 12, 4}));
		assertEquals(6 ,arrTests.maxSpan(new int[] {3, 6, 8, 8, 4, 3, 6, 8}));
	}

	@Test
	public void testEqualSides() {
		arrayChecks arrTests = new arrayChecks();
		assertTrue(arrTests.equalSides(new int[] {3, 0, -1, 2, 1}));
		assertTrue(arrTests.equalSides(new int[] {2, 1, 2, 3, 1, 4}));
		assertFalse(arrTests.equalSides(new int[] {8,8}));
	}

}

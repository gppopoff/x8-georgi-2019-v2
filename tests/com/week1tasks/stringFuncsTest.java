package com.week1tasks;

import static org.junit.Assert.*;

import org.junit.Test;

public class stringFuncsTest {

	@Test
	public void testHistogram_WithBasicMatrix() {
		stringFuncs strTester = new stringFuncs();
		short[][] image = {{1,2,3},{1,2,3},{4,5,6}};
		int[] result = new int[256];
		result[1] = 2;
		result[2] = 2;
		result[3] = 2;
		result[4] = 1;
		result[5] = 1;
		result[6] = 1;
		
		assertArrayEquals(result, strTester.histogram(image));
	}

}

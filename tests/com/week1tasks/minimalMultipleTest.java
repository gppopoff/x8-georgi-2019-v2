package com.week1tasks;

import static org.junit.Assert.*;

import org.junit.*;

public class minimalMultipleTest {

	@Test
	public void testMinMulti() {
		minimalMultiple minTester = new minimalMultiple();
		assertEquals(840,minTester.minMulti(8));
		assertEquals(2520,minTester.minMulti(9));
		assertEquals(2520,minTester.minMulti(10));
		assertEquals(27720,minTester.minMulti(11));
		assertEquals(27720,minTester.minMulti(12));
	}

}

package com.week1tasks;

import static org.junit.Assert.*;

import org.junit.*;

public class TestTest {

	
	@Test
	public void isOddTestExpectedTrue() {
		numberChecks numberTest = new numberChecks();
		assertTrue("is 9 odd number" , numberTest.isOdd(9));
		assertTrue("is 43 odd number" , numberTest.isOdd(43));
	}
	@Test
	public void isOddTestExpectedFalse() {
		numberChecks numberTest = new numberChecks();
		assertFalse("is 8 odd number" , numberTest.isOdd(8));
		assertFalse("is 42 odd number" , numberTest.isOdd(42));
		assertFalse("is 0 odd number" , numberTest.isOdd(0));
	}
	@Test
	public void isPrimeTestExpectedTrue() {
		numberChecks numberTest = new numberChecks();
		assertTrue(numberTest.isPrime(2));
		assertTrue(numberTest.isPrime(3));
		assertTrue(numberTest.isPrime(5));
		assertTrue(numberTest.isPrime(11));
		assertTrue(numberTest.isPrime(29));
	}
	@Test
	public void isPrimeTestExpectedFalse() {
		numberChecks numberTest = new numberChecks();
		assertFalse(numberTest.isPrime(1));
		assertFalse(numberTest.isPrime(0));
		assertFalse(numberTest.isPrime(4));
		assertFalse(numberTest.isPrime(6));
		assertFalse(numberTest.isPrime(9));
	}
	@Test
	public void facTest() {
		numberChecks numberTest = new numberChecks();
		assertEquals( 1, numberTest.Fac(1));
		assertEquals( 2, numberTest.Fac(2));
		assertEquals( 6, numberTest.Fac(3));
		assertEquals( 3628800, numberTest.Fac(10));
	}
	@Test
	public void kFacTest() {
		numberChecks numberTest = new numberChecks();
		assertEquals( 1, numberTest.kFac(1, 3));
		assertEquals( 2, numberTest.kFac(2, 10));
		assertEquals( 720, numberTest.kFac(3, 2));
	}
	
	
}

package com.week1tasks;

import static org.junit.Assert.*;

import org.junit.Test;

public class stringFuncsReversTest {

	@Test
	public void testReverse() {
		stringFuncs strTester = new stringFuncs();
		assertEquals("ParaPa",strTester.reverse("aParaP"));
		assertEquals("",strTester.reverse(""));
		assertEquals("aaa",strTester.reverse("aaa"));
	}

	@Test
	public void testReversWords() {
		stringFuncs strTester = new stringFuncs();
		assertEquals("Tova ne beshe taka!", strTester.reversWords("avoT en ehseb !akat"));
		assertEquals("", strTester.reversWords(""));
		assertEquals("aPaaB", strTester.reversWords("BaaPa"));
		assertEquals("Arr", strTester.reversWords("rrA  "));
	}

}

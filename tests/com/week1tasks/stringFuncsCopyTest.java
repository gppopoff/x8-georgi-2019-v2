package com.week1tasks;

import static org.junit.Assert.*;

import org.junit.Test;

public class stringFuncsCopyTest {

	@Test
	public void testCopyCharsWithRepeats() {
		stringFuncs strTester = new stringFuncs();
		assertEquals("", strTester.copyChars("", 3));
		assertEquals("asdasdasd", strTester.copyChars("asd", 3));
		assertEquals("    ", strTester.copyChars(" ", 4));
		assertEquals("a aa aa aa aa a", strTester.copyChars("a a", 5));
		assertEquals(" a  a  a ", strTester.copyChars(" a ", 3));
	}

	@Test
	public void testCopyCharsWithNoRepeats() {
		stringFuncs strTester = new stringFuncs();
		assertEquals("", strTester.copyChars("", 0));
		assertEquals("", strTester.copyChars("asd", 0));
		assertEquals("", strTester.copyChars(" ", 0));
	}
	@Test
	public void testMentions() {
		stringFuncs strTester = new stringFuncs();
		assertEquals(0, strTester.mentions("", ""));
		assertEquals(0, strTester.mentions("", "asds das das asdas asd "));
		assertEquals(3, strTester.mentions("as", "basbasbbabsbas"));
		assertEquals(5, strTester.mentions(" ", "asds das das asdas asd "));
		assertEquals(5, strTester.mentions(" ", " asds das das asdas asd"));
	}

}

package com.filesystem;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.filesystem.files.FSDirectory;

class TerminalTest {

	@Test
	void test() {
		Terminal test = new Terminal(new FSDirectory("home", new FSDirectory("/")));
		test.run();
	}

}

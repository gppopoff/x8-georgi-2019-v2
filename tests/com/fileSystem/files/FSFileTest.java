package com.filesystem.files;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.filesystem.files.FSLine;
import com.filesystem.files.FSTextFile;

class FSFileTest {

	@Test
	void test_AddLineWithIncorectIndex() {
		FSTextFile tester = new FSTextFile("test");
		tester.addLine(new FSLine(-1, "Test String"));
		assertEquals(0, tester.getSize());
		assertEquals(0, tester.getLastLineIndex());
	}

	@Test
	void test_AddLineWithCorrectIndexAndNewLine() {
		FSTextFile tester = new FSTextFile("test");
		FSLine line = new FSLine(2, "Test String");
		tester.addLine(line);
		assertEquals(new FSLine(2, "Test String"), tester.getLine(2));
		assertEquals(13, tester.getSize());
		assertEquals(2, tester.getLastLineIndex());
		tester.addLine(new FSLine(3, "Second Test Line"));
		assertEquals(new FSLine(3, "Second Test Line"), tester.getLine(3));
		assertEquals(30, tester.getSize());
		assertEquals(3, tester.getLastLineIndex());
	}

	@Test
	void test_AddLineWithCorrectIndexAndExistingLine() {
		FSTextFile tester = new FSTextFile("test");
		FSLine line2 = new FSLine(2, "Test String");
		line2.addContent("Test String");
		tester.addLine(new FSLine(2, "Test String"));
		tester.addLine(new FSLine(2, "Test String"));
		assertEquals(line2, tester.getLine(2));
		assertEquals(24, tester.getSize());
		assertEquals(2, tester.getLastLineIndex());
	}

	@Test
	void test_RemoveLineWithIncorectIndex() {
		FSTextFile tester = new FSTextFile("test");
		tester.addLine(new FSLine(1, "Test String"));
		tester.removeLine(-1);
		assertEquals(12, tester.getSize());
	}

	@Test
	void test_SizeAfretRemoveLineWithLastIndex() {
		FSTextFile tester = new FSTextFile("test");
		tester.addLine(new FSLine(1, "Test String"));
		tester.addLine(new FSLine(5, "Test String"));
		tester.removeLine(5);
		assertEquals(12, tester.getSize());
	}

	@Test
	void test_SizeAfretRemoveLineWithNotLastIndex() {
		FSTextFile tester = new FSTextFile("test");
		tester.addLine(new FSLine(1, "Test String"));
		tester.addLine(new FSLine(5, "Test String"));
		tester.removeLine(1);
		assertEquals(16, tester.getSize());
	}
}

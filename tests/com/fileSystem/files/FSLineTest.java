package com.filesystem.files;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.filesystem.files.FSLine;

class FSLineTest {

	@Test
	void test_getContentWithSetter() {
		FSLine tester = new FSLine();
		tester.setContent("Ima li nqkogo");
		assertEquals("Ima li nqkogo", tester.getContent().toString());
	}

	@Test
	void test_getContentWithConstructor() {
		FSLine tester = new FSLine(1,"Ima li nqkogo");
		assertEquals("Ima li nqkogo", tester.getContent().toString());
	}

	@Test
	void test_numOfCharsWithEmptyString() {
		FSLine tester = new FSLine(1,"");
		assertEquals(0, tester.getNumOfChars());
	}

	@Test
	void test_numOfCharsWithSimpleStringWithoutSpace() {
		FSLine tester = new FSLine(1,"Exo");
		assertEquals(3, tester.getNumOfChars());
	}

	@Test
	void test_numOfCharsWithSimpleStringWithSpace() {
		FSLine tester = new FSLine(1,"Exo kakvo stava");
		assertEquals(15, tester.getNumOfChars());
	}

	@Test
	void test_numOfCharsWithSimpleStringAfterAddingEmptyString() {
		FSLine tester = new FSLine(1,"Exo");
		tester.addContent("");
		assertEquals(3, tester.getNumOfChars());
	}

	@Test
	void test_numOfCharsWithSimpleStringAfterAddingNotEmptyString() {
		FSLine tester = new FSLine(1,"Exo");
		tester.addContent(" kakvo stava");
		assertEquals(15, tester.getNumOfChars());
	}
}

package com.filesystem.files;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.Before;
import org.junit.jupiter.api.Test;

import com.filesystem.files.FSDirectory;
import com.filesystem.files.FSLine;
import com.filesystem.files.FSTextFile;

class FSDirectoryTest {

	@Before
	void set() {
	}

	@Test
	void test_addObjectWithList() {
		FSDirectory home = new FSDirectory("home");
		FSDirectory dir1 = new FSDirectory("dir1", home);
		FSDirectory dir2 = new FSDirectory("dir2", home);
		FSTextFile file1 = new FSTextFile("file1");
		file1.addLine(new FSLine(1, "Kvo staa shefe"));
		file1.addLine(new FSLine(3, "Kaza toi byrzo"));
		FSTextFile file2 = new FSTextFile("file2");

		home.addObject(file1);
		home.addObject(dir2);
		home.addObject(file2);
		home.addObject(dir1);
		dir2.addObject(file2);
		dir1.addObject(file1);

		dir1.printInfo();
		dir1.show();
		dir2.printInfo();
		dir2.show();
		home.printInfo();
		home.show();
//		home.listObjects();
//		home.listSortedObjects();
//		FSFile newOne = (FSFile)home.getObjectByKey("file1");
//		newOne.printFile();
	}

}

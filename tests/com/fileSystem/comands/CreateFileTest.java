package com.filesystem.comands;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.filesystem.files.FSDirectory;
import com.filesystem.files.FSTextFile;

class CreateFileTest {

	FSDirectory base = new FSDirectory("/");
	FSDirectory home = new FSDirectory("home", base);
	FSDirectory currentDir = home;
	FSDirectory dir1 = new FSDirectory("dir1", home);
	Command test = new CreateFile(currentDir);

	@Test
	void test_CreateFileExecuteWithSimpleName() {
		test.execute(new String[] { "create_file", "newFile" });
		assertTrue(currentDir.containsName("newFile"));
		assertTrue(currentDir.getObjectByKey("newFile").getClass() == FSTextFile.class);
		assertEquals(1, currentDir.getObjects().size());
		test.execute(new String[] { "create_file", "newFile2" });
		assertTrue(currentDir.containsName("newFile2"));
		assertTrue(currentDir.getObjectByKey("newFile2").getClass() == FSTextFile.class);
		assertEquals(2, currentDir.getObjects().size());
	}

	@Test
	void test_CreateFileExecuteWithSameName() {
		test.execute(new String[] { "create_file", "newFile" });
		assertTrue(currentDir.containsName("newFile"));
		assertTrue(currentDir.getObjectByKey("newFile").getClass() == FSTextFile.class);
		assertEquals(1, currentDir.getObjects().size());
		test.execute(new String[] { "create_file", "newFile" });
		assertFalse(currentDir.containsName("newFile2"));
		assertEquals(1, currentDir.getObjects().size());
	}

	@Test
	void test_CreateFileExecuteWithNoString() {
		test.execute(new String[] { "create_file", "newFile" });
		assertTrue(currentDir.containsName("newFile"));
		assertTrue(currentDir.getObjectByKey("newFile").getClass() == FSTextFile.class);
		assertEquals(1, currentDir.getObjects().size());
		test.execute(new String[] { "create_file" });
		assertFalse(currentDir.containsName("newFile2"));
		assertEquals(1, currentDir.getObjects().size());
	}
}

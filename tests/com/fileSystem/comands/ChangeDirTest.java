package com.filesystem.comands;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.filesystem.files.FSDirectory;

class ChangeDirTest {

	FSDirectory base = new FSDirectory("/");
	FSDirectory home = new FSDirectory("home", base);
	FSDirectory dir1 = new FSDirectory("dir1", home);
	FSDirectory dir2 = new FSDirectory("dir2", home);
	FSDirectory dir11 = new FSDirectory("dir11", dir1);
	FSDirectory dir12 = new FSDirectory("dir12", dir1);
	FSDirectory dir21 = new FSDirectory("dir21", dir2);
	FSDirectory dir22 = new FSDirectory("dir22", dir2);
	FSDirectory currentDir = home;
	Command test = new ChangeDir(currentDir);

	@BeforeEach
	void set() {
		base.addObject(home);
		home.addObject(dir1);
		home.addObject(dir2);
//		currentDir.addObject(dir1);
//		currentDir.addObject(dir2);
		dir1.addObject(dir11);
		dir1.addObject(dir12);
		dir2.addObject(dir21);
		dir2.addObject(dir22);
	}

	@Test
	void test_ChangeDirWithPreviousFolderCommand() {
		test.execute(new String[] { "cd", ".." });
		currentDir = Command.getCurrentDir();
		assertTrue(currentDir.containsName("home"));
		assertEquals("/", currentDir.getName());
		assertEquals(1, currentDir.getObjects().size());
		currentDir = dir1;
		Command.setDir(dir1);
		test.execute(new String[] { "cd", ".." });
		currentDir = Command.getCurrentDir();
		assertTrue(currentDir.containsName("dir1"));
		assertTrue(currentDir.containsName("dir2"));
		assertEquals("home", currentDir.getName());
		assertEquals(2, currentDir.getObjects().size());
	}

	@Test
	void test_ChangeDirWithPreviousFolderCommandSecond() {
		Command.setDir(dir12);
		test.execute(new String[] { "cd", ".." });
		currentDir = Command.getCurrentDir();
		assertTrue(currentDir.containsName("dir12"));
		assertTrue(currentDir.containsName("dir11"));
		assertEquals("dir1", currentDir.getName());
		assertEquals(2, currentDir.getObjects().size());

		test.execute(new String[] { "cd", ".." });
		currentDir = Command.getCurrentDir();
		assertTrue(currentDir.containsName("dir1"));
		assertTrue(currentDir.containsName("dir2"));
		assertEquals("home", currentDir.getName());
		assertEquals(2, currentDir.getObjects().size());

		test.execute(new String[] { "cd", ".." });
		currentDir = Command.getCurrentDir();
		assertTrue(currentDir.containsName("home"));
		assertEquals("/", currentDir.getName());
		assertEquals(1, currentDir.getObjects().size());
	}

	@Test
	void test_ChangeDirWithValidSubFolder() {
		test.execute(new String[] { "cd", "dir1" });
		currentDir = Command.getCurrentDir();
		assertTrue(currentDir.containsName("dir12"));
		assertTrue(currentDir.containsName("dir11"));
		assertEquals("dir1", currentDir.getName());
		assertEquals(2, currentDir.getObjects().size());

		test.execute(new String[] { "cd", "dir12" });
		currentDir = Command.getCurrentDir();
		assertEquals("dir12", currentDir.getName());
		assertEquals(0, currentDir.getObjects().size());
	}

	@Test
	void test_ChangeDirWithInvalidSubFolder() {
		test.execute(new String[] { "cd", "dir12" });
		currentDir = Command.getCurrentDir();
		assertTrue(currentDir.containsName("dir1"));
		assertTrue(currentDir.containsName("dir2"));
		assertEquals("home", currentDir.getName());
		assertEquals(2, currentDir.getObjects().size());

		test.execute(new String[] { "cd", "/" });
		currentDir = Command.getCurrentDir();
		assertTrue(currentDir.containsName("dir1"));
		assertTrue(currentDir.containsName("dir2"));
		assertEquals("home", currentDir.getName());
		assertEquals(2, currentDir.getObjects().size());
	}

	@Test
	void test_ChangeDirWithInvalidCommand() {
		test.execute(new String[] { "cd" });
		currentDir = Command.getCurrentDir();
		assertTrue(currentDir.containsName("dir1"));
		assertTrue(currentDir.containsName("dir2"));
		assertEquals("home", currentDir.getName());
		assertEquals(2, currentDir.getObjects().size());

		test.execute(new String[] { "cd" });
		currentDir = Command.getCurrentDir();
		assertTrue(currentDir.containsName("dir1"));
		assertTrue(currentDir.containsName("dir2"));
		assertEquals("home", currentDir.getName());
		assertEquals(2, currentDir.getObjects().size());
	}

}

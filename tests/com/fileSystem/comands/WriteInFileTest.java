package com.filesystem.comands;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.filesystem.files.FSDirectory;
import com.filesystem.files.FSTextFile;

class WriteInFileTest {

	FSDirectory base = new FSDirectory("/");
	FSDirectory home = new FSDirectory("home", base);
	FSTextFile text1 = new FSTextFile("text1");
	FSTextFile text2 = new FSTextFile("text2");
	FSDirectory currentDir = home;
	Command test = new WriteInFile(currentDir);

	@BeforeEach
	void set() {
		base.addObject(home);
		home.addObject(text1);
		home.addObject(text2);
	}

	@Test
	void test_WriteInFileWithValidInputOnDifferentLines() {
		test.execute(new String[] { "write", "text1", "1", "abc" });
		assertEquals(4 ,currentDir.getObjectByKey("text1").getSize());
		assertEquals("abc", currentDir.getObjectByKey("text1").show().split("\n")[0]);
		test.execute(new String[] { "write", "text1", "2", "abc" });
		assertEquals(8 ,currentDir.getObjectByKey("text1").getSize());
		assertEquals("abc", currentDir.getObjectByKey("text1").show().split("\n")[1]);
		test.execute(new String[] { "write", "text2", "2", "123" });
		assertEquals(5 ,currentDir.getObjectByKey("text2").getSize());
		assertEquals("123", currentDir.getObjectByKey("text2").show().split("\n")[1]);
		test.execute(new String[] { "write", "text2", "1001", "abc" });
		assertEquals(1007 ,currentDir.getObjectByKey("text2").getSize());
		assertEquals("abc", currentDir.getObjectByKey("text2").show().split("\n")[1000]);
	}

	@Test
	void test_WriteInFileWithValidInputOnSameLines_Concat() {
		test.execute(new String[] { "write", "text1", "1", "abc" });
		test.execute(new String[] { "write", "text1", "1", "abc" });
		assertEquals(7 ,currentDir.getObjectByKey("text1").getSize());
		assertEquals("abcabc", currentDir.getObjectByKey("text1").show().split("\n")[0]);
		test.execute(new String[] { "write", "text2", "1001", "123" });
		test.execute(new String[] { "write", "text2", "1001", "abc" });
		assertEquals(1007 ,currentDir.getObjectByKey("text2").getSize());
		assertEquals("123abc", currentDir.getObjectByKey("text2").show().split("\n")[1000]);
	}

	@Test
	void test_WriteInFileWithValidInputOnSameLines_OverWrite() {
		test.execute(new String[] { "write", "text1", "1", "ccc" });
		test.execute(new String[] { "write", "text1", "1", "abc", "-overwrite" });
		assertEquals(4 ,currentDir.getObjectByKey("text1").getSize());
		assertEquals("abc", currentDir.getObjectByKey("text1").show().split("\n")[0]);
		test.execute(new String[] { "write", "text2", "1001", "123" });
		test.execute(new String[] { "write","-overwrite", "text2", "1001", "abc" });
		assertEquals(1004 ,currentDir.getObjectByKey("text2").getSize());
		assertEquals("abc", currentDir.getObjectByKey("text2").show().split("\n")[1000]);
	}
}

package com.filesystem.comands;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.filesystem.files.FSDirectory;

class MakeDirTest {

	FSDirectory base = new FSDirectory("/");
	FSDirectory home = new FSDirectory("home", base);
	FSDirectory currentDir = home;
	FSDirectory dir1 = new FSDirectory("dir1", home);
	MakeDir test = new MakeDir(currentDir);

	@BeforeEach
	void set() {
		currentDir.addObject(dir1);
	}

	@Test
	void test_executeWithSimpleDir() {
		String[] arg = { "mkdir", "newFold" };
		test.execute(arg);
		assertTrue(currentDir.containsName("newFold"));
		assertEquals(2, currentDir.getObjects().size());
		currentDir.show();
	}

	@Test
	void test_executeWithSameNameDir() {
		String[] arg = { "mkdir", "dir1" };
		test.execute(arg);
		assertEquals(1, currentDir.getObjects().size());
		currentDir.show();
	}

	@Test
	void test_executeWithNoNameDir() {
		String[] arg = { "mkdir", "" };
		test.execute(arg);
		assertEquals(1, currentDir.getObjects().size());
		currentDir.show();
	}

	@Test
	void test_executeWithNoString() {
		String[] arg = { "mkdir" };
		test.execute(arg);
		assertEquals(1, currentDir.getObjects().size());
		currentDir.show();
	}
}

package com.stacks;

import static org.junit.Assert.*;

import org.junit.Test;

public class linkedStackTest {


	@Test
	public void test_PushAndTopWithInt_ExpectEquals() {
		linkedStack stack = new linkedStack();
		stack.push(5);
		assertEquals(5,stack.top());
		int a = 4;
		stack.push(a);
		assertEquals(a,stack.top());
		assertEquals(4,stack.top());
		
	}
	
	@Test
	public void test_PushAndSizeWithInt_ExpectEquals() {
		linkedStack stack = new linkedStack();
		stack.push(5);
		assertEquals(1,stack.size());
		int a = 4;
		stack.push(a);
		assertEquals(2,stack.size());
		
	}
	
	@Test
	public void test_PushAndTopWithString_ExpectEquals() {
		linkedStack stack = new linkedStack();
		stack.push("Neshto");
		assertEquals("Neshto", stack.top());
		String a = "Drugo";
		stack.push(a);
		assertEquals(a,stack.top());
		assertEquals("Drugo", stack.top());
		
	}
	
	@Test
	public void test_PushAndSizeWithString_ExpectEquals() {
		linkedStack stack = new linkedStack();
		stack.push("Neshto");
		assertEquals(1, stack.size());
		String a = "Drugo";
		stack.push(a);
		assertEquals(2, stack.size());
		
	}
	
	@Test
	public void test_PopAndTopWithInt_ExpectEquals() {
		linkedStack stack = new linkedStack();
		stack.push(1);
		stack.push(2);
		stack.push(3);
		stack.push(4);
		
		stack.pop();
		assertEquals(3,stack.top());
		
		stack.pop();
		assertEquals(2,stack.top());
		
		stack.pop();
		assertEquals(1,stack.top());

	}

	@Test
	public void test_PopAndSizeWithInt_ExpectEquals() {
		linkedStack stack = new linkedStack();
		stack.push(1);
		stack.push(2);
		stack.push(3);
		stack.push(4);
		
		stack.pop();
		assertEquals(3,stack.size());
		
		stack.pop();
		assertEquals(2,stack.size());
		
		stack.pop();
		assertEquals(1,stack.size());

	}
	
	@Test
	public void test_Empty_ExpectsFalse() {
		linkedStack stack = new linkedStack();
		stack.push(1);
		stack.push(2);
		stack.push(3);
		stack.push(4);
		
		assertFalse(stack.empty());
		
		stack.pop();
		assertFalse(stack.empty());
		
		stack.pop();
		assertFalse(stack.empty());
		
		stack.pop();
		assertFalse(stack.empty());
	}
	
	@Test
	public void test_Empty_ExpectsTrue() {
		linkedStack stack = new linkedStack();
		assertTrue(stack.empty());
		
	}
	
	@Test
	public void test_EmptyAndPop_ExpectsTrue() {
		linkedStack stack = new linkedStack();
		assertTrue(stack.empty());
		stack.push(1);
		stack.pop();
		assertTrue(stack.empty());
		
	}
}

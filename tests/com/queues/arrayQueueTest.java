package com.queues;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class arrayQueueTest {
	arrayQueue queue = new arrayQueue();

	@Before
	public void pre_test() {
		
		queue.enqueue(0);
		queue.enqueue(1);
		queue.enqueue(2);
		queue.enqueue(3);
		queue.enqueue(4);
		queue.enqueue(5);
		
	}
	
	@Test 
	public void test_EnqueueWithDequeue() {
		assertEquals(0, queue.dequeue());
		assertEquals(1, queue.dequeue());
		assertEquals(2, queue.dequeue());
		assertEquals(3, queue.dequeue());
		assertEquals(4, queue.dequeue());
		assertEquals(5, queue.dequeue());
	}
	
	@Test 
	public void test_Size() {
		assertEquals(6,queue.size());
		queue.dequeue();
		assertEquals(5,queue.size());
		queue.dequeue();
		assertEquals(4,queue.size());
		queue.dequeue();
		assertEquals(3,queue.size());
		queue.dequeue();
		assertEquals(2,queue.size());
		queue.dequeue();
		assertEquals(1,queue.size());
		queue.dequeue();
		assertEquals(0,queue.size());
	}
	
	@Test 
	public void test_Resize() {
		queue.dequeue();
		queue.dequeue();
		queue.enqueue(1);
		queue.enqueue(2);
		queue.enqueue(3);
		queue.enqueue(4);
		queue.enqueue(5);

		assertEquals(2, queue.dequeue());
		assertEquals(3, queue.dequeue());
		assertEquals(4, queue.dequeue());
		assertEquals(5, queue.dequeue());
		assertEquals(1, queue.dequeue());
		assertEquals(2, queue.dequeue());
		assertEquals(3, queue.dequeue());
		assertEquals(4
				, queue.dequeue());
		assertEquals(5, queue.dequeue());
	}

}
